from django.forms import ModelForm, HiddenInput

from .models import Cache, Reaction


class CacheCreateForm(ModelForm):
    'Form used to create a cache.'

    class Meta:
        model = Cache
        fields = ['title', 'description', 'location']
        widgets = {
            'location': HiddenInput()
        }


class ReactionCreateForm(ModelForm):
    'A form used to add a reaction to a cache.'

    class Meta:
        model = Reaction
        fields = ['found', 'comment']
