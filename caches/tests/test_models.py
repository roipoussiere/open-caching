from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from django.contrib.auth import get_user_model

from caches.models import Cache


User = get_user_model()


class CacheModelTests(TestCase):
    'Cache model test case'

    def test_cache_creation(self):
        'Newly created cache contains correct fields.'

        cache = Cache(
            creator=User(username='user 1'),
            title='title 1',
            description='description 1'
        )
        time_delta = timezone.now() - cache.creation_date

        self.assertIs(cache.creator.username, 'user 1')
        self.assertIs(cache.title, 'title 1')
        self.assertIs(cache.description, 'description 1')
        self.assertTrue(time_delta > timedelta(seconds=0) and time_delta < timedelta(seconds=1))
        self.assertIs(cache.nb_times_found, 0)
