# OpenCaching

A simple project similar to geocaching.com, created to learn Django.

![](./images/demo_home.png)

![](./images/demo_details.png)
