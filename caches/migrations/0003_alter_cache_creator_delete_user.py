# Generated by Django 4.2.5 on 2023-10-03 08:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('caches', '0002_rename_nbtimesfound_cache_nb_times_found_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cache',
            name='creator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
