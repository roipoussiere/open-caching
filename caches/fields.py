from django.core.exceptions import ValidationError
from django.db.models import CharField


class LocationField(CharField):
    '''A field used to define a location coordinates based on a string with comma-separated
    latitude/longitude values (ex: `"43.5749,1.4083"`).'''

    def __init__(self, *args, **kwargs):
        # kwargs['widget'] = forms.HiddenInput()
        kwargs['max_length'] = 40
        super().__init__(*args, **kwargs)

    def validate(self, value, model_instance):
        'Check if value consists only of valid emails.'

        if not value:
            raise ValidationError('Please select a location', 'empty')

        try:
            lat, lng = [float(nb) for nb in value.split(',')]
        except ValueError as error:
            raise ValidationError('invalid location string', 'bad_string') from error

        if not (-90 <= lat <= 90 and 0 <= lng <= 180):
            raise ValidationError('invalid location coordinates', 'bad_coord')
