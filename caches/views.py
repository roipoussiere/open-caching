from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import CacheCreateForm, ReactionCreateForm
from .models import Cache, Reaction


class CacheListView(ListView):
    'View to list caches'
    def get_queryset(self):
        return Cache.objects.order_by('-creation_date')[:50]


class CacheDetailView(LoginRequiredMixin, DetailView):
    'View to show cache detail'
    model = Cache


class CacheCreateView(LoginRequiredMixin, CreateView):
    'View to add a cache'

    model = Cache
    form_class = CacheCreateForm

    def form_valid(self, form):
        form.instance.creator = self.request.user
        return super().form_valid(form)


class ReactionCreateView(LoginRequiredMixin, CreateView):
    'View to add a reaction to a cache'

    model = Reaction
    form_class = ReactionCreateForm

    def form_valid(self, form):
        print('\n\n=== pk =', self.kwargs['pk'], ' ===\n\n')
        form.instance.author = self.request.user
        form.instance.target = Cache.objects.get(pk=self.kwargs['pk'])
        form.instance.found = 'found' in self.request.POST
        self.update_counter(form.instance.found, form.instance.target)
        return super().form_valid(form)

    def update_counter(self, is_found: bool, target):
        '''Increment the correct target counter, wich is:
        `nb_times_found` if the cache has been marked as found in the reaction,
        or `nb_times_not_found` otherwise.'''

        if is_found:
            target.nb_times_found += 1
        else:
            target.nb_times_not_found += 1
        target.save()
