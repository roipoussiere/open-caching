const marker = L.marker();

window.addEventListener('load', (event) => {
	map_dom = document.getElementsByClassName('leaflet-container')[0];
	console.log(map_dom);
	const options = {
		minZoom: 3,
		maxZoom: 18,
	}
	const map = L.map(map_dom).setView([43.574925, 1.408308], 13);
	L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', options).addTo(map);

	if(map_dom.id === 'map-cache-list') {
		initMapCacheList(map);
	} else if (map_dom.id === 'map-add-cache') {
		initMapAddCache(map);
	} else if (map_dom.id === 'map-cache-detail') {
		initMapCacheDetail(map);
	}
});

function getLoc(node) {
	return node.dataset.loc.split(',').map((loc) => parseFloat(loc.trim()));
}

function initMapCacheList(map) {
	for (const li of document.getElementById('cache-list').children) {
		L.marker(getLoc(li))
			.bindTooltip(li.dataset.title)
			.on('click', () => {
				window.location.href = `/${li.dataset.id}`;
			})
			.addTo(map);
	}
}

function initMapCacheDetail(map) {
	const cache_info = document.getElementById('cache-info');
	const location = getLoc(cache_info);
	map.setView(location);
	L.marker(location)
		.bindTooltip(cache_info.dataset.title)
		.addTo(map);
}

function initMapAddCache(map) {
	map.on('click', (event) => {
		const isNewMarker = marker._latlng === undefined;
		marker.setLatLng(event.latlng);
		if (isNewMarker) {
			marker.addTo(event.target);
		}
		document.getElementById('id_location').value = `${marker._latlng.lat},${marker._latlng.lng}`;
	});
}
