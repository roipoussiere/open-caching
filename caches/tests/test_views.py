from django.urls import reverse
from django.test import TestCase
from django.contrib.auth import get_user_model

from caches.models import Cache


User = get_user_model()


def create_user(user_name: str):
    'Utility function to create a user with the given user name.'
    return User.objects.create(username=user_name)

def create_cache(user: User, title: str, description: str): # pyright: ignore
    'Utility function to create a cache with the given user, title and description.'
    return Cache.objects.create(creator=user, title=title, description=description)


class IndexViewTests(TestCase):
    'IndexView test case'

    def test_no_cache(self):
        'If no cache exist, an appropriate message is displayed.'

        response = self.client.get(reverse('caches:index'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No caches available')
        self.assertQuerySetEqual(response.context['cache_list'], [])

    def test_cache(self):
        'If a cache exist, return the list containing the cache.'

        user = create_user('user 2')
        cache = create_cache(user, 'title 2', 'description 2')
        response = self.client.get(reverse('caches:index'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'List of caches')
        self.assertQuerySetEqual(response.context['cache_list'], [cache])


class DetailViewTests(TestCase):
    'DetailView test case'

    def test_unauthorized(self):
        'Non-logged users accessing a detail view are redirected to the login page.'

        response = self.client.get(reverse('caches:detail', args=(1,)))
        self.assertEqual(response.status_code, 302)

        response = self.client.get(reverse('caches:detail', args=(1,)), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Log in')

    def test_invalid_cache(self):
        'The detail view of a non-existing cache returns a 404 not found.'

        user = create_user('user 3')
        self.client.force_login(user)
        response = self.client.get(reverse('caches:detail', args=(1,)), follow=True)

        self.assertEqual(response.status_code, 404)
