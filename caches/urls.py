from django.urls import path

from . import views


app_name = 'caches'

urlpatterns = [
    path('', views.CacheListView.as_view(), name='cacheList'),
    path('<int:pk>/', views.CacheDetailView.as_view(), name='cacheDetail'),
    path('add/', views.CacheCreateView.as_view(), name='cacheCreate'),

    path('<int:pk>/add', views.ReactionCreateView.as_view(), name='reactionCreate'),
]
