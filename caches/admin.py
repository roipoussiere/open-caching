from django.contrib import admin

from .models import Cache, Reaction


class CacheAdmin(admin.ModelAdmin):
    'Cache model admin configuration'

    list_display = ['title', 'creator', 'nb_times_found', 'nb_times_not_found', 'creation_date']
    list_filter = ['creation_date']
    search_fields = ['title']


class ReactionAdmin(admin.ModelAdmin):
    'CacheReaction model admin configuration'

    list_display = ['comment', 'target', 'found', 'author', 'creation_date']
    list_filter = ['target', 'creation_date']
    search_fields = ['title']


admin.site.register(Cache, CacheAdmin)
admin.site.register(Reaction, ReactionAdmin)

admin.site.site_header = 'OpenCache admin'
