from django.db import models
from django.conf import settings
from django.utils.timezone import now
from django.urls import reverse

from caches.fields import LocationField


class Cache(models.Model):
    'Cache model'

    title = models.CharField(max_length=50)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    nb_times_found = models.IntegerField(default=0)
    nb_times_not_found = models.IntegerField(default=0)
    description = models.CharField(max_length=250)
    creation_date = models.DateTimeField(default=now)
    location = LocationField()

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        'get absolute url'
        return reverse('caches:cacheDetail', kwargs={'pk': self.pk})


class Reaction(models.Model):
    'Reaction model'

    target = models.ForeignKey(Cache, on_delete=models.CASCADE)
    found = models.BooleanField()
    comment = models.CharField(max_length=250)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(default=now)

    def __str__(self):
        return str(self.comment)[:30]

    def get_absolute_url(self):
        'get absolute url'
        return reverse('caches:cacheDetail', kwargs={'pk': self.target.id})
